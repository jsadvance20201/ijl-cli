import { i18nextInitConfig, i18nextReactInitConfig } from './src/i18next';

const {
  getConfig,
  getConfigValue,
  getNavigations,
  getNavigationsValue,
  getAllFeatures,
  getFeatures,
  getHistory,
} = System.get("root.scope");

export { 
  getConfig, 
  getConfigValue, 
  getNavigations, 
  getNavigationsValue, 
  getAllFeatures, 
  getFeatures,
  i18nextInitConfig,
  i18nextReactInitConfig,
  getHistory,
};
