const pkg = require("./package.json");

module.exports = {
  apiPath: "example/stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${pkg.version}/`
    }
  },
  config: {},
  navigations: {},
  features: {
    ijl: {
      firstFeature: true,
      secondFeature: {
        value: true,
        options: {
          firstOptions: true
        }
      }
    }
  }
};
