import React from 'react'
import {Icon} from './assets'
import { useTranslation } from "react-i18next";

const App = () => {
    const {t, i18n} = useTranslation()

    const handleChangeLangToEn = async () => {
       await i18n.changeLanguage('en');
    }

    const handleChangeLangToRu = async () => {
       await i18n.changeLanguage('ru');
    }

    return(<div>
        Hello world
        <div>{t('ijl.cli.main.title')}</div>
        <div>{t('ijl.cli.main.header')}</div>
        <img src={Icon} alt=""/>
        <button onClick={handleChangeLangToEn}>English</button>
        <button onClick={handleChangeLangToRu}>Russian</button>
    </div>)
}

export default App;
