declare module '*.svg' {
    const value: string;
    const ReactComponent: any;

    export default value;
    export { ReactComponent }
}
