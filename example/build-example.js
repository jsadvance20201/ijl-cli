process.env.NODE_ENV = "development";
process.env.ENTRY = "example/src/index.tsx"
process.env.LOCALES = "./example/locales";

require('../src/webpack/build')()
