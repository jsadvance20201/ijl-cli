import { i18n } from 'i18next';
import { History } from 'history';

type AppKey = string;
type FeatureName = string;
type Feature = { on: boolean, value: string, key: FeatureName };

type TGetConfig = () => Record<string, string>;
type TGetConfigValue = (configKey: string) => string;
type TGetNavigations = () => Record<string, string>;
type TGetNavigationsValue = (navKey: string) => string;
type TI18nextInitConfig = (i18next: i18n) => Promise<undefined>;
type TI18nextReactInitConfig = (i18next: i18n) => Promise<undefined>;
type TGetAllFeatures = () => Record<AppKey, Record<FeatureName, Feature>>;
type TGetFeatures = (pkgName: string) => Record<FeatureName, Feature>;
type TGetHistory = () => History;

export const getConfig: TGetConfig;
export const getConfigValue: TGetConfigValue;
export const getNavigations: TGetNavigations;
export const getNavigationsValue: TGetNavigationsValue;

export const i18nextInitConfig: TI18nextInitConfig;
export const i18nextReactInitConfig: TI18nextReactInitConfig;
export const getAllFeatures: TGetAllFeatures;
export const getFeatures: TGetFeatures;
export const getHistory: TGetHistory;
