const base = require("./base");
const {
    merge
} = require("webpack-merge");

const webpack = require("webpack");
const path = require("path");

const outputDirectory = "dist";

module.exports = merge({
        mode: "development",
        entry: {
            index: [
                'webpack-hot-middleware/client',
                // 'react-hot-loader/patch',
                /**
                 * Отключено так как не совместимо с 6 версией react-router-dom
                 * 
                 * rrd проверяет тип детей роутеов,
                 * ожидает соответствие своему типу.
                 * а rhl оборачивает все компоненты в свой ProxyFacade
                 * из-за этого тип не соответствует и все падает
                 */
                path.resolve(process.env.ENTRY)
            ]
        },
        output: {
            filename: "[name].js",
            path: path.resolve(process.cwd(), outputDirectory),
            libraryTarget: "umd",
            globalObject: `(typeof self !== 'undefined' ? self : this)`
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.DefinePlugin({
                "typeof window": JSON.stringify("object")
            })
        ],
        devtool: "source-map"
    },

    base
);