const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpackCopy = require("copy-webpack-plugin");
const { CustomizeRule, mergeWithRules } = require("webpack-merge");

const isProd = process.env.NODE_ENV === 'production';

const getProjectConfig = require("../../server/utils/get-project-config");

const { webpackConfig = {} } = getProjectConfig();

const outputDirectory = "dist";

const cwd = process.cwd();

const result = mergeWithRules({
  module: {
    rules: {
      test: CustomizeRule.Match,
      use: {
        loader: CustomizeRule.Match,
        options: CustomizeRule.Replace
      }
    }
  }
})(
  {
    output: {
      filename: "[name].js?[hash]",
      path: path.resolve(cwd, outputDirectory),
      libraryTarget: "umd",
      globalObject: `(typeof self !== 'undefined' ? self : this)`
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpackCopy({
        patterns: [
          { from: "./remote-assets/", to: "remote-assets", noErrorOnMissing: true },
          { from: process.env.LOCALES, to: "locales", noErrorOnMissing: true }
        ],
        
      })
    ],
    resolve: {
      modules: [cwd, "node_modules"],
      extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },

    module: {
      rules: [
        {
            test: /\.(jsx?|tsx?)$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader",
                options: {
                    presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript']
                }
            }
        },
        {
          test: /\.(jpe?g|gif|png|woff|woff2|svg|ttf|eot|wav|mp3|webp|ico)$/,
          loader: "file-loader"
        },
        {
          test: /\.css$/i,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader',
            },
          ],
        },
      ]
    },
  },
  webpackConfig
);

module.exports = result;