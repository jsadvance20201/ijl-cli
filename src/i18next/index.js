import axios from "axios";
import { initReactI18next } from "react-i18next";
import XHR from "i18next-xhr-backend";
import LanguageDetector from 'i18next-browser-languagedetector';


const loadPath = () => `${__webpack_public_path__}locales/{{lng}}.json`;

const i18nextAxios = axios.create();

const ajax = (url, opts, callback) =>
  i18nextAxios(url).then((response) => {
    const enrichedLocales = {
      ...response.data,
    };
    callback(JSON.stringify(enrichedLocales), response);
  });

export const i18nextInitConfig = (i18next) => i18next.use(XHR).init({
  lng: localStorage.getItem('i18nextLng') || "ru",
  fallbackLng: "ru",
  keySeparator: false,
  backend: {
    loadPath,
    ajax,
  },
});

export const i18nextReactInitConfig = (i18next) => i18next.use(XHR).use(initReactI18next).use(LanguageDetector).init({
  fallbackLng: "ru",
  load: 'currentOnly',
  keySeparator: false,
  whitelist: ['ru', 'en'],
  backend: {
    loadPath,
    ajax,
  },
});

