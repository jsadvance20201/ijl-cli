const {
    name
} = require('../../server/utils/get-module-name')();

module.exports = () => `const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: \`/static/\${pkg.name}/\${process.env.VERSION || pkg.version}/\`
        }
    },
    navigations: {
        '${name}.main': '/${name}',
        'link.${name}.auth': '/auth'
    },
    features: {
        '${name}': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        '${name}.api': '/api',
    }
}
`