const {
    name
} = require('../../../server/utils/get-module-name')();

module.exports = () => `import React from 'react';

const App = () => {
    return(
        <h1>Hello world для проекта - ${name}</h1>
    )
}

export default App;

`