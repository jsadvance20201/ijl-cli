const path = require('path');
const mkdirp = require('mkdirp');
const chalk = require('chalk');
const {
    readdir,
    stat,
    writeFile
} = require('fs/promises');

const logSuccess = (...messages) => console.log(chalk.green(...messages));
const logError = (...messages) => console.log(chalk.red(...messages));
const logWarning = (...messages) => console.log(chalk.keyword('orange')(...messages));

let templatesPathRoot;

const scanAndCreate = async (currentPath) => {
    try {
        const files = await readdir(currentPath, {
            encoding: 'utf-8',
            withFileTypes: true
        });

        for (const fileOrDir of files) {
            if (fileOrDir.isDirectory()) {
                scanAndCreate(`${currentPath}/${fileOrDir.name}`);
            } else {
                const cleanFileName = fileOrDir.name.replace(/.js$/, '');
                const cleanFilePath = `${currentPath.replace(templatesPathRoot, '.')}/${cleanFileName}`;

                try {
                    await stat(path.resolve(cleanFilePath))

                    logWarning(`${cleanFileName} Уже существует, пропускаю этот шаг.`);
                } catch (error) {
                    const init = require(`${currentPath}/${fileOrDir.name}`);

                    const dir = currentPath.replace(`${templatesPathRoot}/`, '');
                    if (dir) {
                        await mkdirp(dir);
                    }
                    await writeFile(
                        path.resolve(cleanFilePath),
                        init()
                    );

                    logSuccess(`${cleanFileName} успешно создан.`)
                }
            }
        }
    } catch (error) {
        logError(error.message);
    }
}

const main = async (advanced) => {
    templatesPathRoot = path.resolve(__dirname, advanced ? 'templates-advanced' : 'templates');
    try {
        await scanAndCreate(templatesPathRoot);


        const packagePath = path.resolve("package");
        const package = require(packagePath);

        package.main = './src/index.tsx';
        package.scripts = package.scripts || {}
        package.scripts.start = 'ijl-cli --server --port=8099 --with-open-browser'
        package.scripts.build = 'npm run clean && ijl-cli --build --dev'
        package.scripts['build:prod'] = "npm run clean && ijl-cli --build",
            package.scripts.clean = 'rimraf dist'

        logSuccess('Обновляю package.json ...')
        await writeFile(packagePath + '.json', JSON.stringify(package, null, 4));

        logSuccess('Устанавливаю необходимые зависимости ...')
        require('child_process').execSync(
            `npm i --save react@17 react-dom@17 express typescript @types/react@17 @types/react-dom@17 ${advanced ? 'react-router-dom' : ''}`, {
                stdio: 'inherit'
            }
        );

        logSuccess(`
Все готово
Наберите команду npm start
`)
    } catch (error) {
        logError(error.message);
    }

}

if (!module.parent) {
    main();
}

module.exports = main;