const init = require('./init')

module.exports = (program) => {
    if (program.init) {
        init(program.advanced)
    }
}