const express = require("express");
const webpack = require("webpack");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const webpackDevMiddleware = require("webpack-dev-middleware");
const webpackHotMiddleware = require("webpack-hot-middleware");
const openBrowser = require("react-dev-utils/openBrowser");
const { applyHbs } = require("@ijl/templates");
const { setIo, getIo } = require('./io');
const path = require('path')

const getProjectConfig = require("./utils/get-project-config");
const { entryPoint, cleanName: name } = require("./utils/get-module-name")();

if (!process.env.ENTRY) {
  process.env.ENTRY = entryPoint;
}

const getModuleApi = require("./utils/get-module-api");
const getConfig = require("./utils/get-config-template");

const getmoduleMiddleware = require("../../middleware/get-module");

const {
  apiPath = "stubs/api",
  config = {},
  navigations = {},
  apps = {},
  features = {}
} = getProjectConfig();

module.exports = ({ port, withOpenBrowser }) => {
  const app = express();
  applyHbs(app);
  const server = setIo(app);
  app.use(bodyParser.json({ limit: '50mb' }))
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
  app.use(session({
    secret: 'LONG_RANDOM_STRING_HERE',
    resave: true,
    saveUninitialized: false
  }));
  app.use(cookieParser());

  const appPath = `/${name}`;

  // hot reload -->
  const webpackConfig = require("../webpack/development");
  const compiler = webpack(webpackConfig);

  app.use(
    webpackDevMiddleware(compiler, {
      publicPath: webpackConfig.output.publicPath
    })
  );
  
 app.use(webpackHotMiddleware(compiler));
  // <-- hot reload

  app.get("/", (req, res) => res.redirect(appPath));

  getModuleApi(app, apiPath);

  const envConfig = getConfig({ config, navigations, apps, features });
  app.use(appPath, function(request, response) {
    response.render("dev.hbs", envConfig);
  });

  app.use(
    envConfig.baseUrl,
    express.Router().get(/\/([.-\w]+)\/([.-\w\d]+)\/(.*)/, getmoduleMiddleware)
  );

  server.listen(port, () => {
    const openUrl = `http://localhost:${port}${appPath}`;
    console.log("🛠", "Open:", openUrl);

    if (withOpenBrowser) {
      openBrowser(openUrl);
    }
  });
};

module.exports.getIo = getIo;
