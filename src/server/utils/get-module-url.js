const path = require('path');

module.exports = (moduleName, version, filePath) => {
    const modulePath = path.resolve('node_modules', moduleName, filePath);

    return modulePath;
}
