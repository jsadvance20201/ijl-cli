const chalk = require('chalk');
const watch = require('recursive-watch')

const invalidateApi = require('./invalidate-api');
const safeConnectApi = require('./safe-connect-api');

let watchId = null;
module.exports = (app, location) => {
    const watchApi = () => {
        clearTimeout(watchId);
        watchId = setTimeout(() => {
            invalidateApi(location);
            safeConnectApi(app, location);
            console.log(chalk.green('api updated'));
        }, 200);
    }
    safeConnectApi(app, location);

    watch(location, watchApi);

}
