const path = require('path');
const locatePath = require('locate-path').sync;
const fs = require('fs');

module.exports = () => {
  const serverRcPath = locatePath([
    path.resolve("ijl.config.js"),
    path.resolve("ijl.config.json")
  ]);

  if (serverRcPath) {
      return require(serverRcPath);
  }

  const configNoExt = locatePath([path.resolve(".ijlconfigrc")]);

  if (configNoExt) {
      return JSON.parse(fs.readFileSync(configNoExt, 'utf-8'));
  }

  return {};
};
