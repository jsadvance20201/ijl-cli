const {
  apps: appsBase,
  config: configBase,
  navigations: navigationsBase,
  ...rest
} = require("../templates");

module.exports = ({ apps, config, navigations, features }) => {
  return {
    apps: { ...appsBase, ...apps },
    features,
    config: { ...configBase, ...config },
    navigations: { ...navigationsBase, ...navigations },
    ...rest
  };
};
