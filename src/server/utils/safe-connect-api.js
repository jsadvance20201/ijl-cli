module.exports = (app, location) => {
    app.use('/api', (...args) => require(location).apply(app, args));
}
