const defaultConfig = require("./config")

module.exports = {
  baseUrl: defaultConfig.baseUrl,
  pageTitle: "Наш Сервис",
  apps: require("./app"),
  navigations: require("./navigations"),
  config: defaultConfig,
  fireappVersion: '1.0.0'
};