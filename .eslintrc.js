module.exports = {
    env: {
        node: true,
        commonjs: true,
        es2021: true
    },
    ignorePatterns: ['src/i18next/**', 'index.js'],
    extends: 'eslint:recommended',
    parserOptions: {
        ecmaVersion: 12
    },
    rules: {
    },
    globals: {
    }
};
